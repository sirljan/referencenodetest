//
//  GameScene.swift
//  ReferenceNodeTest
//
//  Created by Jan Širl on 04/04/16.
//  Copyright (c) 2016 Jan Širl. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        let myLabel = SKLabelNode(fontNamed:"Chalkduster")
        myLabel.text = "Hello, World!"
        myLabel.fontSize = 45
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        
        self.addChild(myLabel)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        
        if let nextPart = SKReferenceNode(fileNamed: "SecondPartScene") {
            if self.childNodeWithName("SecondPartScene") == nil {
                self.addChild(nextPart)
            }
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
